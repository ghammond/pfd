'''
Emily 10.9.17
Calculate lumped parameters from observation points
  1. Volume-averaged liquid pressure
  2. Brine volume
  3. volume-averaged gas saturation
  4. volume-averaged liquid saturation
  5. volume-averaged porosity
for
  rWAS_AREA
  rSROR
  rNROR
  rOPS_AREA
  rEXP_AREA
  rSPCS
  rMPCS
  rNPCS
Usage is limited to a single pflotran-obs-*.tec file.
'''

import sys
import os
import numpy

def LumpedParams(rlist):
  infilename = sys.argv[1] #works for one obs file
  outfilename = infilename.split('.')[0] + '.lumped'
  if len(sys.argv) != 2:
    print('ERROR: command line arguments not provided.\n')
    print('Usage: python lumped_params.py pflotran-obs-0.tec')
    sys.exit(0)

  print('Infile: {} Outfile: {}'.format(infilename,outfilename))

  with open(infilename,'r') as f:
    header = f.readline()
  headings = header.split(',')
  headings_out = ['"Time [y]"'] #will append to this

  #loadtxt results in rows of data, not columns, very confusing
  data = numpy.loadtxt(infilename,skiprows=1) #returns ndarray
  ntimes = data.shape[0] #length of row in data
  print('In LumpedParams(), ntimes = {}'.format(ntimes))
  nouts = 5 #per region
  data_out = numpy.zeros((ntimes,1+len(rlist)*nouts),'=f8')
  data_out[:,0] = data[:,0] #copy time

  counter = 1 #because time is in 0th column
  for region in rlist:
    headings_out.extend(['"VA Liquid Pressure [Pa] {}"'.format(region),
                         '"VA Liquid Saturation {}"'.format(region),
                         '"VA Gas Saturation {}"'.format(region),
                         '"VA Effective Porosity {}"'.format(region),
                         '"Brine Volume {}"'.format(region),
                         ]
                        )
    lp_ids = [] #list of columns with liquid pressure
    ls_ids = [] #liquid saturation
    gs_ids = [] #gas saturation
    ep_ids = [] #effective porosity
    cv_ids = [] #cell volume
    index = 0
    for heading in headings:
      words = heading.split()
      if any(word == region for word in words):
        if words[0] == '"Liquid' and words[1] == 'Pressure':
          lp_ids.append(index)
        elif words[0] == '"Liquid' and words[1] == 'Saturation':
          ls_ids.append(index)
        elif words[0] == '"Gas' and words[1] == 'Saturation':
          gs_ids.append(index)
        elif words[0] == '"Effective' and words[1] == 'Porosity':
          ep_ids.append(index)
        elif words[0] == '"Volume':
          cv_ids.append(index)
      index += 1
    if (len(lp_ids) <= 0 or len(ls_ids) <= 0 or len(gs_ids) <= 0 
        or len(ep_ids) <=0 or len(cv_ids) <= 0): #if any missing
      print('Error: Region {} or dataset not found in {}. Stopping'.format(
            region,infilename))
      sys.exit(0)
    #Total volume
    tot_vol = 0.
    for col_id in cv_ids:
      tot_vol += data[0][col_id]
    #column id offsets
    lp_dif = cv_ids[0] - lp_ids[0]
    ls_dif = cv_ids[0] - ls_ids[0]
    gs_dif = cv_ids[0] - gs_ids[0]
    ep_dif = cv_ids[0] - ep_ids[0]
    ep_dif2 = ls_ids[0] - ep_ids[0]
    #Volume-averaged liquid pressure
    tot = numpy.zeros((ntimes,),'=f8')
    for col_id in lp_ids:
      tot += data[:,col_id] * data[:,col_id+lp_dif]
    data_out[:,counter] = tot/tot_vol
    #Volume-averaged liquid saturation
    #(algebra calc's as 1 - vol_ave_gas_saturation)
    tot = numpy.zeros((ntimes,),'=f8')
    for col_id in ls_ids:
      tot += data[:,col_id] * data[:,col_id+ls_dif]
    data_out[:,counter+1] = tot/tot_vol
    #Volume-averaged gas saturation
    tot = numpy.zeros((ntimes,),'=f8')
    for col_id in gs_ids:
      tot += data[:,col_id] * data[:,col_id+gs_dif]
    data_out[:,counter+2] = tot/tot_vol
    #Volume-averaged effective porosity and total brine volume
    tot_ep = numpy.zeros((ntimes,),'=f8')
    tot_lv = numpy.zeros((ntimes,),'=f8')
    for col_id in ep_ids:
      tot_ep += data[:,col_id] * data[:,col_id+ep_dif]
      tot_lv += data[:,col_id] * data[:,col_id+ep_dif] \
                * data[:,col_id+ep_dif2]
    data_out[:,counter+3] = tot_ep/tot_vol
    data_out[:,counter+4] = tot_lv
    counter += nouts

  string = ','.join(heading for heading in headings_out)
  numpy.savetxt(outfilename,data_out,fmt='%.6e',
                header=string,comments='')

  return

if __name__ == '__main__':
  rlist = ['rWAS_AREA',
           'rSROR',
           'rNROR',
           'rOPS_AREA',
           'rEXP_AREA',
           'rSPCS',
           'rMPCS',
           'rNPCS',
          ]
  LumpedParams(rlist)
  print('done') 
