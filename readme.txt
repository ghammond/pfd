Instructions for running the BRAGFLO - PFLOTRAN comparison.

NOVEMBER 9, 2017
It is NECESSARY to run prepflotran with desired vectors and scenarios,
before running 2d-flared test cases. If you don't, you will not have a grid!
Run control can be parallel or serial.
prePFLOTRAN produces a bash shell script that will run time-5y scenario for
each vector, run restart0y_v{}.py for each vector, and run each vector for
each scenario (s1, s2, etc.).
NOTE: The only post-processing included in runcontrol_parallel.sh and
run_control_serial.sh is calculation of volume-averaged quantities using
lumped_params.py.

0. Install parallel:
  a.) sudo apt-get install parallel (or whatever the command is on Solaris)
  b.) please let me know if parallel isn't an option

1. Using SSH:
  a.) git clone git@gitlab-ex.sandia.gov:wipp/pflotran-bragflo-2d-flared.git
  b.) git clone git@gitlab-ex.sandia.gov:wipp/prepflotran.git

2. Set up PREPFLOTRAN_DIR and PFLOTRAN_DIR
  a.) export PREPFLOTRAN_DIR=/path/to/your/prepflotran/repository
  b.) export PFLOTRAN_DIR=/path/to/your/pflotran/repository
(You can add those lines to your .bashrc or .bash_profile and never have to
 export them again.)

3. In your prepflotran/ directory,
  a.) cd test/
  b.) python ../src/prepflotran.py

4. In your pflotran-bragflo-2d-flared/ directory:
  a.) ./copy_input.sh
  b.) ./runcontrol_parallel.sh OR ./runcontrol_serial.sh

5. At this point in time, comparison to bragflo is set up for vector 1 only:
  a.) cd ../example_bragflo
  b.) Example bragflo output is provided: bf2_PFD_r1_s1_v001.h5
      and bf2_PFD_r1_s2_v001.h5. Edit the file names in setup_s1_diff.sh and
      setup_s2_diff.sh if yours are different. 
  c.) ./setup_s1_diff.sh
  d.) ./setup_s2_diff.sh

6. Viewing PFLOTRAN results:
  a.) edit setup_xmf.sh to create xmf files for any
      vectors you'd like to plot in Paraview.

7. Viewing time step history:
  a.) edit setup_timestep.sh to plot time steps as fn of time for any vectors

8. Calculating and plotting volume-averaged quantities:
  a.) runcontrol.sh will have run lumped_params.py for each simulation
  c.) edit plot_lumped.py to plot the results

