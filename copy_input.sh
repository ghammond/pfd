#!/bin/bash 
#Edit the path to bash if necessary for your machine.

#Add to your .bashrc or .bash_profile or to this script:
#export PREPFLOTRAN_DIR=/path/to/your/prepflotran/repository

cp $PREPFLOTRAN_DIR/test/runcontrol_parallel.sh .
cp $PREPFLOTRAN_DIR/test/runcontrol_serial.sh .
cp -r $PREPFLOTRAN_DIR/test/grid .
cp -r $PREPFLOTRAN_DIR/test/initcond .
cp -r $PREPFLOTRAN_DIR/test/restart .
cp $PREPFLOTRAN_DIR/test/pflotran_closure.dat .
cp -r $PREPFLOTRAN_DIR/test/time-5y .
cp -r $PREPFLOTRAN_DIR/test/s1 .
cp -r $PREPFLOTRAN_DIR/test/s2 .
cp -r $PREPFLOTRAN_DIR/test/s3 .
cp -r $PREPFLOTRAN_DIR/test/s4 .
cp -r $PREPFLOTRAN_DIR/test/s5 .
cp -r $PREPFLOTRAN_DIR/test/s6 .

rm time-5y/time-5y.py
rm s1/s1.py
rm s2/s2.py
rm s3/s3.py
rm s4/s4.py
rm s5/s5.py
rm s6/s6.py
