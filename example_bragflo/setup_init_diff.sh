#! /bin/bash 
python difference.py bf2_init_PFD_r1_s2_v001.h5 ../time-5y/time-5y_v1_pflotran.h5
#use this one for real dimensions:
#python ../../write_xmf.py bf2_init_PFD_r1_s2_v001_difference ../grid/wipp-domain-dip
#use this one for equal area visualization:
python ../write_xmf.py bf2_init_PFD_r1_s2_v001_difference ../grid/domain-elev-equal
