
#!/bin/bash
#Edit the path to bash if necessary for your machine.

#Add to your .bashrc or .bash_profile or to this script:
#export PFLOTRAN_DIR=/path/to/your/pflotran/repository

cd time-5y
parallel 'echo "time-5y_v{1}_pflotran.in complete"; $PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix time-5y_v{1}_pflotran >& time-5y_v{1}_pflotran.stdout;  cp time-5y_v{1}_pflotran-restart.h5 ../restart;  cd ../restart;  python restart0y_v{1}.py;  cd ../time-5y;' ::: 1

cd ..

#for generality decided to allow rerun of time-5y/
#you could delete it from list for efficiency
parallel 'cd {1}; echo "{1} v{2} complete"; $PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix {1}_v{2}_pflotran >& {1}_v{2}_pflotran.stdout; python ../lumped_params.py {1}_v{2}_pflotran-obs-0.tec; cd ..' ::: time-5y s1 s2 s3 s4 s5 s6 ::: 1
      
#does not include post-processing other than lumped_parameters.py,
#which calculates volume-averaged saturations, pressures, etc.
