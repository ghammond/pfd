
'''
modify checkpoint file produced by t = -5 to 0 run for restart at 0 y
'''

import numpy as np
from h5py import *

chptfile = 'time-5y_v1_pflotran-restart.h5' #copy from time-5y directory

#regions to reset
fnwas_area = '../grid/rWAS_AREA.txt'
fnreposit  = '../grid/rREPOSIT.txt'
fnops_area = '../grid/rOPS_AREA.txt'
fnexp_area = '../grid/rEXP_AREA.txt'
fnconc_mon = '../grid/rCONC_MON.txt'
fnshftl    = '../grid/rSHFTL.txt'
fnshftu    = '../grid/rSHFTU.txt'
#need to reset permeability and porosity for all the regions in which
#material is going to change at t = 0. In addition to the above:
fnculebra  = '../grid/rCULEBRA.txt'
fnfortynin = '../grid/rFORTYNIN.txt'
fndewylake = '../grid/rDEWYLAKE.txt'
fnsantaros = '../grid/rSANTAROS.txt'
fntamarisk = '../grid/rTAMARISK.txt'
fnmagenta  = '../grid/rMAGENTA.txt'
fndrz      = '../grid/rDRZ.txt'
fndrz_oe   = '../grid/rDRZ_OE.txt'
fndrz_pcs  = '../grid/rDRZ_PCS.txt'
fnpcs      = '../grid/rPCS.txt'

ref_pres =   1.0132500e+05 #BRINESAL.ref_pres
wa_pres  =   1.2803900e+05 #CAVITY_1.pressure
rep_pres =   1.2803900e+05 #CAVITY_2.pressure
#these are all gas saturations
wa_sg    =   9.8500000e-01 #1-WAS_AREA.sat_ibrn
rep_sg   =   9.8500000e-01 #1-REPOSIT.sat_ibrn
pcs_sg   =   4.3358144e-01 #1-PCS_T1.sat_rbrn (sampled)
shft_sg  =   1.0000000e-07 #1-CONC_MON.sat_ibrn
oe_sg    =   1.0000000e+00 #1-OPS_AREA.sat_ibrn

wa_por  =   8.4800000e-01 #WAS_AREA.porosity
wa_k    =   2.3999379e-13 #10^WAS_AREA.prmx_log
rep_por =   8.4800000e-01 #REPOSIT.porosity
rep_k   =   2.3999379e-13 #10^REPOSIT.prmx_log
ops_por =   1.8000000e-01 #OPS_AREA.porosity
ops_k   =   1.0000000e-11 #10^OPS_AREA.prmx_log
exp_por =   1.8000000e-01 #EXP_AREA.porosity
exp_k   =   1.0000000e-11 #10^EXP_AREA.prmx_log
cm_por  =   5.0000000e-02 #CONC_MON.porosity
cm_k    =   1.0000000e-14 #10^CONC_MON.prmx_log
sl_por  =   1.1300000e-01 #SHFTL_T1.porosity
sl_k    =   1.5632320e-19 #10^SHFTL_T1.prmx_log (sampled)
su_por  =   2.9100000e-01 #SHFTU.porosity
su_k    =   4.2985575e-19 #10^SHFTU.prmx_log (sampled)
cul_por =   1.5100000e-01 #CULEBRA.porosity
cul_k   =   9.5940063e-15 #10^CULEBRA.prmx_log
for_por =   8.2000000e-02 #FORTYNIN.porosity
for_k   =   1.0000000e-35 #10^FORTYNIN.prmx_log
dew_por =   1.4300000e-01 #DEWYLAKE.porosity
dew_k   =   5.0118723e-17 #10^DEWYLAKE.prmx_log
san_por =   1.7500000e-01 #SANTAROS.porosity
san_k   =   1.0000000e-10 #10^SANTAROS.prmx_log
tam_por =   6.4000000e-02 #TAMARISK.porosity
tam_k   =   1.0000000e-35 #10^TAMARISK.prmx_log
mag_por =   1.3800000e-01 #MAGENTA.porosity
mag_k   =   2.0989399e-15 #10^MAGENTA.prmx_log
drz_por =   7.3823916e-03 #DRZ_1.porosity (sampled)
drz_k   =   1.3340318e-18 #10^DRZ_1.prmx_log (sampled)
drz_pcs_por =   7.3823916e-03 #DRZ_PC_1.porosity (sampled)
drz_pcs_k   =   1.3340318e-18 #10^DRZ_PC_1.prmx_log (sampled)
drz_oe_por  =   7.3823916e-03 #DRZ_OE_1.porosity (sampled)
drz_oe_k    =   1.3340318e-18 #10^DRZ_OE_1.prmx_log (sampled)
pcs_por     =   1.3379778e-01 #PCS_T1.porosity (sampled)
pcs_k       =   5.5760226e-18 #10^PCS_T1.prmx_log (sampled)

h5 = File(chptfile,'r+')
#Reset initial pressure and saturation, porosity and permeability 
icp_chpt = np.array(h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:],'=f8')
por = np.array(h5['Checkpoint/PMCSubsurface/flow/Porosity'][:],'=f8')
permx = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_X'][:],'=f8')
permy = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_Y'][:],'=f8')
permz = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_Z'][:],'=f8')
f = open(fnwas_area,'r') #nonzero initial brine saturation
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = wa_pres
  icp_chpt[index+1] = wa_sg
  por[int(line)-1] = wa_por
  permx[int(line)-1] = wa_k
  permy[int(line)-1] = wa_k
  permz[int(line)-1] = wa_k
f.close()
f = open(fnreposit,'r') #nonzero initial brine saturation
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = rep_pres
  icp_chpt[index+1] = rep_sg
  por[int(line)-1] = rep_por
  permx[int(line)-1] = rep_k
  permy[int(line)-1] = rep_k
  permz[int(line)-1] = rep_k
f.close()
f = open(fnops_area,'r') #"zero" initial brine saturation
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = ref_pres
  icp_chpt[index+1] = oe_sg
  por[int(line)-1] = ops_por
  permx[int(line)-1] = ops_k
  permy[int(line)-1] = ops_k
  permz[int(line)-1] = ops_k
f.close()
f = open(fnexp_area,'r') #"zero" initial brine saturation
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = ref_pres
  icp_chpt[index+1] = oe_sg
  por[int(line)-1] = exp_por
  permx[int(line)-1] = exp_k
  permy[int(line)-1] = exp_k
  permz[int(line)-1] = exp_k
f.close()
f = open(fnconc_mon,'r') #initial brine saturation of 1, with atm P
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = ref_pres
  icp_chpt[index+1] = shft_sg
  por[int(line)-1] = cm_por
  permx[int(line)-1] = cm_k
  permy[int(line)-1] = cm_k
  permz[int(line)-1] = cm_k
f.close()
f = open(fnshftl,'r') #initial brine saturation of 1, with atm P
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = ref_pres
  icp_chpt[index+1] = shft_sg
  por[int(line)-1] = sl_por
  permx[int(line)-1] = sl_k
  permy[int(line)-1] = sl_k
  permz[int(line)-1] = sl_k
f.close()
f = open(fnshftu,'r') #initial brine saturation of 1, with atm P
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = ref_pres
  icp_chpt[index+1] = shft_sg
  por[int(line)-1] = su_por
  permx[int(line)-1] = su_k
  permy[int(line)-1] = su_k
  permz[int(line)-1] = su_k
f.close()
#modify only porosity and permeability
f = open(fnculebra,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = cul_por
  permx[int(line)-1] = cul_k
  permy[int(line)-1] = cul_k
  permz[int(line)-1] = cul_k
f.close()
f = open(fnfortynin,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = for_por
  permx[int(line)-1] = for_k
  permy[int(line)-1] = for_k
  permz[int(line)-1] = for_k
f.close()
f = open(fndewylake,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = dew_por
  permx[int(line)-1] = dew_k
  permy[int(line)-1] = dew_k
  permz[int(line)-1] = dew_k
f.close()
f = open(fnsantaros,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = san_por
  permx[int(line)-1] = san_k
  permy[int(line)-1] = san_k
  permz[int(line)-1] = san_k
f.close()
f = open(fntamarisk,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = tam_por
  permx[int(line)-1] = tam_k
  permy[int(line)-1] = tam_k
  permz[int(line)-1] = tam_k
f.close()
f = open(fnmagenta,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = mag_por
  permx[int(line)-1] = mag_k
  permy[int(line)-1] = mag_k
  permz[int(line)-1] = mag_k
f.close()
f = open(fndrz,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = drz_por
  permx[int(line)-1] = drz_k
  permy[int(line)-1] = drz_k
  permz[int(line)-1] = drz_k
f.close()
f = open(fndrz_oe,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = drz_oe_por
  permx[int(line)-1] = drz_oe_k
  permy[int(line)-1] = drz_oe_k
  permz[int(line)-1] = drz_oe_k
f.close()
f = open(fndrz_pcs,'r')
for line in f:
  index = (int(line)-1)*2
  por[int(line)-1] = drz_por
  permx[int(line)-1] = drz_pcs_k
  permy[int(line)-1] = drz_pcs_k
  permz[int(line)-1] = drz_pcs_k
f.close()
f = open(fnpcs,'r') #non-zero initial brine saturation
for line in f:
  index = (int(line)-1)*2
  icp_chpt[index] = ref_pres
  icp_chpt[index+1] = pcs_sg
  por[int(line)-1] = pcs_por
  permx[int(line)-1] = pcs_k
  permy[int(line)-1] = pcs_k
  permz[int(line)-1] = pcs_k
f.close()

h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:] = icp_chpt
h5['Checkpoint/PMCSubsurface/flow/Porosity'][:] = por
h5['Checkpoint/PMCSubsurface/flow/Permeability_X'][:] = permx
h5['Checkpoint/PMCSubsurface/flow/Permeability_Y'][:] = permy
h5['Checkpoint/PMCSubsurface/flow/Permeability_Z'][:] = permz
h5.close()
