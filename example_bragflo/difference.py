'''
Open bragflo results and pflotran results.
Calculate difference.
Save difference to new .h5 file, cell-indexed
Emily 8.17.17
'''

import numpy as np
from h5py import *
import sys

def bragflo_pflotran_diff():

  bragflo_filename = sys.argv[1]
  pflotran_filename = sys.argv[2]
  outfilename = bragflo_filename.split('.')[0] + '_difference.h5'

  #BRAGFLO names
  lpname = 'PRESBRIN'
  gpname = 'PRESGAS'
  cpname = 'PCGW'
  lsname = 'SATBRINE'
  gsname = 'SATGAS'
  ldname = 'DENBRINE'
  gdname = 'DENGAS'
  pornam = 'POROS'

  #PFLOTRAN names
  lppflo = 'Liquid Pressure [Pa]'
  gppflo = 'Gas Pressure [Pa]'
  cppflo = 'Capillary Pressure [Pa]'
  lspflo = 'Liquid Saturation'
  gspflo = 'Gas Saturation'
  ldpflo = 'Liquid Density [kg_m^3]'
  gdpflo = 'Gas Density [kg_m^3]'
  porpfl = 'Effective Porosity'
  matpfl = 'Material ID'

  #open files
  h5bf = File(bragflo_filename,'r')
  h5pf = File(pflotran_filename,'r')
  h5out = File(outfilename,'w')

  print('calculating differences')
  #Read in only arrays I care about. In a loop through time.
  times = h5bf.keys()
  count = 0
  for tkey in times:
    if tkey.startswith('Time'):
      bf_liq_p = np.array(h5bf['{}/{}'.format(tkey,lpname)][:][:],'=f8')
      bf_gas_p = np.array(h5bf['{}/{}'.format(tkey,gpname)][:][:],'=f8')
      bf_cap_p = np.array(h5bf['{}/{}'.format(tkey,cpname)][:][:],'=f8')
      bf_liq_s = np.array(h5bf['{}/{}'.format(tkey,lsname)][:][:],'=f8')
      bf_gas_s = np.array(h5bf['{}/{}'.format(tkey,gsname)][:][:],'=f8')
      bf_liq_d = np.array(h5bf['{}/{}'.format(tkey,ldname)][:][:],'=f8')
      bf_gas_d = np.array(h5bf['{}/{}'.format(tkey,gdname)][:][:],'=f8')
      bf_poros = np.array(h5bf['{}/{}'.format(tkey,pornam)][:][:],'=f8')
      ndown = len(bf_liq_p[:])
      nacross = len(bf_liq_p[0][:])
      #make cell-indexed arrays
      ci_liq_p = np.zeros((ndown*nacross),'=f8')
      ci_gas_p = np.zeros((ndown*nacross),'=f8')
      ci_cap_p = np.zeros((ndown*nacross),'=f8')
      ci_liq_s = np.zeros((ndown*nacross),'=f8')
      ci_gas_s = np.zeros((ndown*nacross),'=f8')
      ci_liq_d = np.zeros((ndown*nacross),'=f8')
      ci_gas_d = np.zeros((ndown*nacross),'=f8')
      ci_poros = np.zeros((ndown*nacross),'=f8')
      for j in xrange(nacross):
        for i in xrange(ndown):
          index = i + j*ndown
          ci_liq_p[index] = bf_liq_p[i][j]
          ci_gas_p[index] = bf_gas_p[i][j]
          ci_cap_p[index] = bf_cap_p[i][j]
          ci_liq_s[index] = bf_liq_s[i][j]
          ci_gas_s[index] = bf_gas_s[i][j]
          ci_liq_d[index] = bf_liq_d[i][j]
          ci_gas_d[index] = bf_gas_d[i][j]
          ci_poros[index] = bf_poros[i][j]
      #Find equivalent pflotran times
      time = float(tkey.split()[1])
      ptimes = h5pf.keys()
      for pkey in ptimes:
        if float(pkey.split()[2]) == time:
          print(pkey)
          p_liq_p = np.array(h5pf['{}/{}'.format(pkey,lppflo)],'=f8')
          p_gas_p = np.array(h5pf['{}/{}'.format(pkey,gppflo)],'=f8')
          p_cap_p = np.array(h5pf['{}/{}'.format(pkey,cppflo)],'=f8')
          p_liq_s = np.array(h5pf['{}/{}'.format(pkey,lspflo)],'=f8')
          p_gas_s = np.array(h5pf['{}/{}'.format(pkey,gspflo)],'=f8')
          p_liq_d = np.array(h5pf['{}/{}'.format(pkey,ldpflo)],'=f8')
          p_gas_d = np.array(h5pf['{}/{}'.format(pkey,gdpflo)],'=f8')
          p_poros = np.array(h5pf['{}/{}'.format(pkey,porpfl)],'=f8')
          p_mater = np.array(h5pf['{}/{}'.format(pkey,matpfl)],'=i4')
      #calculate differences
      d_liq_p = ci_liq_p - p_liq_p
      d_gas_p = ci_gas_p - p_gas_p
      d_cap_p = ci_cap_p - p_cap_p
      d_liq_s = ci_liq_s - p_liq_s
      d_gas_s = ci_gas_s - p_gas_s
      d_liq_d = ci_liq_d - p_liq_d
      d_gas_d = ci_gas_d - p_gas_d
      d_poros = ci_poros - p_poros
      #calculate percent differences
      d100_liq_p = d_liq_p/ci_liq_p * 100.
      d100_gas_p = d_gas_p/ci_gas_p * 100.
      d100_cap_p = d_cap_p/ci_cap_p * 100.
      d100_liq_s = d_liq_s/ci_liq_s * 100.
      d100_gas_s = d_gas_s/ci_gas_s * 100.
      d100_liq_d = d_liq_d/ci_liq_d * 100.
      d100_gas_d = d_gas_d/ci_gas_d * 100.
      d100_poros = d_poros/ci_poros * 100.
      #write to new h5 file
      year = tkey.split()[1]
      #differences
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,lpname),data=d_liq_p)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,gpname),data=d_gas_p)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,cpname),data=d_cap_p)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,lsname),data=d_liq_s)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,gsname),data=d_gas_s)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,ldname),data=d_liq_d)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,gdname),data=d_gas_d)
      h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                            count,year,pornam),data=d_poros)
      #percent differences
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,lpname),data=d100_liq_p)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,gpname),data=d100_gas_p)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,cpname),data=d100_cap_p)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,lsname),data=d100_liq_s)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,gsname),data=d100_gas_s)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,ldname),data=d100_liq_d)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,gdname),data=d100_gas_d)
      h5out.create_dataset('{} Time {} y/{}_Percent_Diff'.format(
                            count,year,pornam),data=d100_poros)
      #materials
      h5out.create_dataset('{} Time {} y/{}'.format(
                            count,year,matpfl),data=p_mater)
      count += 1

  #close files  
  h5bf.close()
  h5pf.close()
  h5out.close()
  print('done')

if __name__ == '__main__':
  bragflo_pflotran_diff()



