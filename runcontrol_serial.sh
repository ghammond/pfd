
#!/bin/bash
#Edit the path to bash if necessary for your machine.

#Add to your .bashrc or .bash_profile or to this script:
#export PFLOTRAN_DIR=/path/to/your/pflotran/repository

cd time-5y
for v in {1}; do
  echo "Running time-5y_v${v}_pflotran.in"
  $PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix time-5y_v${v}_pflotran >& time-5y_v${v}_pflotran.stdout
  cp time-5y_v${v}_pflotran-restart.h5 ../restart
  cd ../restart
  python restart0y_v${v}.py
  cd ../time-5y
done

cd ..

for s in {time-5y,s1,s2,s3,s4,s5,s6}; do
  cd $s
    for v in {1}; do
      echo 'Running '${s}'_v'${v}'_pflotran.in'
      $PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix ${s}_v${v}_pflotran >& ${s}_v${v}_pflotran.stdout
      python ../lumped_params.py ${s}_v${v}_pflotran-obs-0.tec
    done
  cd ..
done
